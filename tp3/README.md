# TP3

## I. Service SSH

### 1. Analyse du service

#### S'assurer que le service sshd est démarré

```bash
[nino@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset:>
     Active: active (running) since Mon 2024-01-29 12:14:05 CET; 5min ago
```

#### Analyser les processus liés au service SSH

```bash
[nino@localhost ~]$ ps -ef | grep sshd
root         692       1  0 12:14 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root        1294     692  0 12:19 ?        00:00:00 sshd: nino [priv]
nino       1311    1294  0 12:19 ?        00:00:00 sshd: nino@pts/0
```

#### Consulter les logs du service SSH

```bash
[nino@localhost ~]$  journalctl -u sshd
Jan 29 13:32:06 localhost systemd[1]: Starting OpenSSH server daemon...
Jan 29 13:32:06 localhost sshd[692]: main: sshd: ssh-rsa algorithm is disab>
Jan 29 13:32:06 localhost sshd[692]: Server listening on 0.0.0.0 port 22.
Jan 29 13:32:06 localhost sshd[692]: Server listening on :: port 22.
Jan 29 13:32:06 localhost systemd[1]: Started OpenSSH server daemon.
Jan 29 13:32:18 localhost.localdomain sshd[1282]: main: sshd: ssh-rsa algor>
Jan 29 13:32:20 localhost.localdomain sshd[1282]: Accepted password for dia>
Jan 29 13:32:20 localhost.localdomain sshd[1282]: pam_unix(sshd:session): s>
lines 1-8/8 (END)
[nino@localhost ~]$ sudo tail /var/log/secure -n 2
[sudo] password for nino:
Jan 29 14:30:25 localhost sshd[1510]: Accepted password for nino from 10.2.1.1 port 53379 ssh2
Jan 29 14:30:25 localhost sshd[1510]: pam_unix(sshd:session): session opened for user nino(uid=1000) by (uid=0)
```

## 2. Modification du service

#### Identifier le fichier de configuration du serveur SSH

```bash
[nino@localhost ~]$ cat /etc/ssh/sshd_config
```

#### Redémarrer le service

```bash
[nino@localhost ~]$ sudo systemctl restart sshd
```

## II. Service HTTP

### 1. Mise en place

#### Installer le serveur NGINX

```bash
[nino@localhost ~]$ sudo dnf install nginx -y
```

#### Démarrer le service NGINX

```bash
[nino@localhost ~]$ sudo systemctl start nginx
```

#### Déterminer sur quel port tourne NGINX

```bash
[nino@localhost ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=1431,fd=6),("nginx",pid=1430,fd=6))
LISTEN 0      511             [::]:80            [::]:*    users:(("nginx",pid=1431,fd=7),("nginx",pid=1430,fd=7))
[nino@localhost ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[nino@localhost ~]$ sudo firewall-cmd --reload
success
```

#### Déterminer les processus liés au service NGINX

```bash
[nino@localhost ~]$ ps -ef | grep nginx
root        1430       1  0 10:35 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1431    1430  0 10:35 ?        00:00:00 nginx: worker process
```

#### Déterminer le nom de l'utilisateur qui lance NGINX

```bash
[nino@localhost ~]$ ps -ef | grep nginx
root        1430       1  0 10:36 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1431    1430  0 10:36 ?        00:00:00 nginx: worker process
[nino@localhost ~]$ sudo cat /etc/passwd | grep nginx
nginx:x:991:991:Nginx web server:/var/lib/nginx:/sbin/nologin
```

## III. Your own services

### 2. Analyse des services existants

#### Afficher le fichier de service SSH

```bash
[nino@localhost ~]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (🌟/usr/lib/systemd/system/sshd.service🌟; enabled; preset:>
[nino@localhost ~]$ sudo cat /usr/lib/systemd/system/sshd.service | grep ExecStart=
ExecStart=/usr/sbin/sshd -D $OPTIONS
```

#### Afficher le fichier de service NGINX

```bash
[nino@localhost ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (🌟/usr/lib/systemd/system/nginx.service🌟; enabled; preset>
[nino@localhost ~]$ sudo cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx
```

### 3. Création de service

#### Créez le fichier /etc/systemd/system/tp3_nc.service

```bash
[nino@localhost ~]$ echo $RANDOM
23358
[nino@localhost ~]$ sudo cat /etc/systemd/system/tp3_nc.service
[Unit]
Description=Super netcat tout fou
[Service]
ExecStart=/usr/bin/nc -l 23358 -k
[nino@localhost ~]$ sudo firewall-cmd --add-port=23358/tcp --permanent
success
[nino@localhost ~]$ sudo firewall-cmd --reload
success
```

🌞 Indiquer au système qu'on a modifié les fichiers de service

```bash
[nino@localhost ~]$ sudo systemctl daemon-reload
```

🌞 Démarrer notre service de ouf

```bash
[nino@localhost ~]$ sudo systemctl start tp3_nc
```

🌞 Vérifier que ça fonctionne

```bash
[nino@localhost ~]$ sudo systemctl status tp3_nc
● tp3_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp3_nc.service; static)
     Active: active (running) since Mon 2024-01-29 16:32:21 CET; 34s ago
[nino@localhost ~]$ ss -laputen | grep tp3
tcp   LISTEN 0      10              0.0.0.0:23358      0.0.0.0:*     ino:28668 sk:4a cgroup:/system.slice/tp3_nc.service <->
tcp   LISTEN 0      10                 [::]:23358         [::]:*     ino:28667 sk:4b cgroup:/system.slice/tp3_nc.service v6only:1 <->

# Sur une autre VM
[nino@meow ~]$ nc 10.2.1.2 23358
coucou
meow
 
```

#### Les logs de votre service

```bash
[nino@localhost ~]$ sudo journalctl -xe -u tp3_nc | grep Started
Jan 29 10:32:21 localhost.localdomain systemd[1]: Started Super netcat tout fou.

[nino@localhost ~]$ sudo journalctl -xe -u tp3_nc | grep nc
Jan 29 11:41:20 localhost.localdomain nc[1941]: coucou
Jan 29 11:42:03 localhost.localdomain nc[1941]: meow
```

#### S'amuser à kill le processus

```bash
[nino@localhost ~]$ ps -ef | grep nc
dbus         677       1  0 11:57 ?        00:00:00 /usr/bin/dbus-broker-lanch --scope system --audit
root        🌟2062🌟       1  0 12:18 ?        00:00:00 /usr/bin/nc -l 23358 -k
[nino@localhost ~]$ sudo kill 2062
```

