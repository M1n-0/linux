#!/bin/bash

echo Machine_name: $(hostnamectl | grep Static | cut -d' ' -f4)

echo OS: $(cat /etc/os-release)

echo OS_version: $(uname -s -rv)

echo IP: $( ip a | head -n 12)

echo RAM : $(free -h --giga)

echo Disk : $(df -h / | grep / | tr -s ' ' | cut -d ' ' -f4) space left.

echo "Top 5 processes by RAM usage:"
ps aux --sort=-%mem | head -6

echo Listening ports :
ss -tulneH | while read ss; do
    port=$(echo "$ss" | tr -s ' ' | cut -d ' ' -f5 | grep -v "::" | cut -d ":" -f2)
    protocol=$(echo "$ss" | tr -s ' ' | grep -v "::" | cut -d ' ' -f1)
    program=$(echo "$ss" | tr -s ' ' | cut -d ' ' -f9 | cut -d '/' -f3 | cut -d '.' -f1)
    echo "      - $port $protocol : $program"
done

echo "PATH directories :"
echo "$PATH" | tr ':' '\n' | while read -r directory; do
    echo "  - $directory"
done

Here is your random cat (jpg file) : https://....
