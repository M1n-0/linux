# TP2

# Partie 1 : Files and users

## I. Fichiers / 1. Find me

### Chemin vers le répertoire personnel

```bash
[nino@localhost /]$ cd home/aymeric/
[nino@localhost ~]$ pwd
> /home/nino
```

### Chemin du fichier de logs SSH
```bash
[nino@localhost /]$ cat /etc/ssh/ssh_config
```

### Chemin du fichier de configuration du serveur SSH
```bash
[nino@localhost /]$ cd /etc/ssh/ssh_config.d
```
## II. Users

## 1. Nouveau user

### nouvel utilisateur
```bash
[nino@localhost /]$ sudo useradd -d /home/papier_alu -p password marmotte
[nino@localhost /]$ sudo passwd marmotte
```

## 2. Infos enregistrées par le système

### Prouver que cet utilisateur a été créé
```bash
[nino@localhost /]$ cat /etc/passwd | grep "marmotte"
> marmotte:x:1001:1002::/home/papier_alu:/bin/bash
```

## 3. Hint sur la ligne de commande

### Tapez une commande pour vous déconnecter : fermer votre session utilisateur

```bash
[nino@localhost /]$ exit
```

### Assurez-vous que vous pouvez vous connecter en tant que l'utilisateur marmotte
```bash
[marmotte@localhost ~]$ cd /home/nino/
> cd: cannot open directory '/home/nino/': Permission denied
```

## 3. Find paths

### Trouver le chemin où est stocké le programme sleep

```bash
[nino@localhost /]$ ls -a /bin | grep sleep
sleep
```

### Tant qu'on est à chercher des chemins : trouver les chemins vers tous les fichiers qui s'appellent .bashrc
```bash
[nino@localhost /]$ sudo find / -name "*.bashrc"
/etc/skel/.bashrc
/root/.bashrc
/home/nino/.bashrc
/home/marmotte/.bashrc
/home/papier_alu/.bashrc
```

### Vérifier que les commandes sleep, ssh, et ping sont bien des programmes stockés dans l'un des dossiers listés dans votre PATH

```bash
[nino@localhost /]$ echo $PATH
/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
[nino@localhost /]$ which sleep
/usr/bin/sleep
[nino@localhost /]$ which ssh
/usr/bin/ssh
[nino@localhost /]$ which ping
/usr/bin/ping
```

## II. Paquets

### Installer le paquet git

```bash
[nino@localhost ~]$ sudo dnf install git
```

## II. Paquets

###  Chemin où est stocké la commande

```bash
[nino@localhost /]$ ls -a /bin | grep git
git
git-receive-pack
git-shell
git-upload-archive
git-upload-pack
```

### Installer le paquet nginx

```bash
[nino@localhost /]$ sudo dnf install nginx
```
### Déterminer le chemin vers le dossier de logs de NGINX
```bash
[nino@localhost /]$ sudo cat /var/log/nginx/
```
### Déterminer le chemin vers le dossier qui contient la configuration de NGINX
```bash
[nino@localhost /]$ cat /etc/nginx/nginx.conf
```